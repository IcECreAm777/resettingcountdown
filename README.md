# ResettingCountdown

A resetting countdown for streams

You can configure up to three values you can add and subtract to the timer while it is running.
The first value you configure can be accessed by hot keys (ctrl + o for add; ctrl + p for subtract).
The timer repeats itself until you stop it manually. It plays a sound when it reaches 0 and resets itself to the reset value at the bottom.
The timer will write the time into a file in the bin directory.

Download the Release.zip to use the Software (you don't need to download the whole project).

You can change the sound of the timer by replacing the 'Alert.wav', but make sure it's still named 'Alert.wav'

# Credits

## Programming

IcELremius (repository owner)

## Testing

MetroTrains

## Sound 

"UI Confirmation Alert, D1.wav" by InspectorJ (www.jshaw.co.uk) of Freesound.org
https://freesound.org/s/413749/
