﻿using System;
using System.IO;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using Path = System.IO.Path;

namespace ResettingCountdown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int _seconds;
        private readonly DispatcherTimer _timer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
        private readonly SoundPlayer _player;
        private FileStream _file;

        private int _firstAmount = 300;
        private int _secondAmount = 60;
        private int _thirdAmount = 1;

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);
        
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        
        private const uint ModNone = 0x0000; //[NONE]
        private const uint ModAlt = 0x0001; //ALT
        private const uint ModControl = 0x0002; //CTRL
        private const uint ModShift = 0x0004; //SHIFT
        private const uint ModWin = 0x0008; //WINDOWS

        private const int ButtonOneAdd = 9000;
        private const int ButtonOneDown = 9002;
        private const int Up5Min = 9001;
        private const int Down5Min = 9003;

        private const uint VkCapitalUp = 0x4F; // O
        private const uint VkCapitalDown = 0x50; // P
        
        private IntPtr _windowHandle;
        private HwndSource _source;
        
        private int Seconds
        {
            get => _seconds;
            set
            {
                _seconds = value;
                
                if (_seconds <= 0)
                {
                    _seconds = BaseTimeMinutes * 60 + BaseTimeSeconds;
                    _player.Play();
                }

                var minutes = _seconds / 60;
                var seconds = _seconds % 60;
                var outString = $"{minutes:00}:{seconds:00}";
                
                LblTime.Content = outString;

                var bytes = new UTF8Encoding(true).GetBytes(outString);

                if (!_file.CanRead || !_file.CanWrite) return;
                _file.Position = 0;
                _file.Flush();
                _file.Write(bytes, 0, bytes.Length);
                _file.SetLength(bytes.Length);
            }
        }

        private int FirstValue
        {
            get => _firstAmount;
            set
            {
                _firstAmount = value;
                
                var minutes = _firstAmount / 60;
                var seconds = _firstAmount % 60;

                BtnOneAdd.Content = $"Add {minutes} min {seconds} sec";
                BtnOneSub.Content = $"Subtract {minutes} min {seconds} sec";
            }
        }

        private int SecondValue
        {
            get => _secondAmount;
            set
            {
                _secondAmount = value;
                
                var minutes = _secondAmount / 60;
                var seconds = _secondAmount % 60;
                
                BtnTwoAdd.Content = $"Add {minutes} min {seconds} sec";
                BtnTwoSub.Content = $"Subtract {minutes} min {seconds} sec";
            }
        }
        
        private int ThirdValue
        {
            get => _thirdAmount;
            set
            {
                _thirdAmount = value;
                
                var minutes = _thirdAmount / 60;
                var seconds = _thirdAmount % 60;
                
                BtnThreeAdd.Content = $"Add {minutes} min {seconds} sec";
                BtnThreeSub.Content = $"Subtract {minutes} min {seconds} sec";
            }
        }

        private int BaseTimeMinutes { get; set; }
        private int BaseTimeSeconds { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            
            _timer.Tick += TimerTick;

            var currentDir = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = Path.Combine(currentDir, @"./Alert.wav");
            _player = new SoundPlayer(Path.GetFullPath(filePath));

            TxbFirstVal.Text = "300";
            TxbSecondVal.Text = "60";
            TxbThirdVal.Text = "1";

            TxbMinutes.Text = "10";
            TxbSeconds.Text = "0";
        }
        
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
  
            _windowHandle = new WindowInteropHelper(this).Handle;
            _source = HwndSource.FromHwnd(_windowHandle);
            _source?.AddHook(HwndHook);

            RegisterHotKey(_windowHandle, ButtonOneAdd, ModControl, VkCapitalUp); //CTRL + O
            RegisterHotKey(_windowHandle, ButtonOneDown, ModControl, VkCapitalDown); //CTRL + P
        }
        
        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(HwndHook);
            UnregisterHotKey(_windowHandle, ButtonOneAdd);
            base.OnClosed(e);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            Seconds--;
        }

        private void TxbMinutes_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(TxbMinutes.Text, out var result))
            {
                BaseTimeMinutes = result;
                return;
            }

            BaseTimeMinutes = 0;
        }

        private void TxbSeconds_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(TxbSeconds.Text, out var result))
            {
                BaseTimeSeconds = result;
                return;
            }

            BaseTimeSeconds = 0;
        }

        private void BtnStartTimer_OnClick(object sender, RoutedEventArgs e)
        {
            BtnStartTimer.Visibility = Visibility.Collapsed;
            BtnStopTimer.Visibility = Visibility.Visible;

            _file = new FileStream("./Time.txt", FileMode.OpenOrCreate);
            
            _timer.Start();
        }

        private void BtnStopTimer_OnClick(object sender, RoutedEventArgs e)
        {
            BtnStartTimer.Visibility = Visibility.Visible;
            BtnStopTimer.Visibility = Visibility.Collapsed;
            
            _timer.Stop();
            
            _file.Close();

            Seconds = 0;
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int wmHotkey = 0x0312;
            switch (msg)
            {
                case wmHotkey:
                    switch (wParam.ToInt32())
                    {
                        case ButtonOneAdd:
                        {
                            var vkey = ((int)lParam >> 16) & 0xFFFF;
                            if (vkey == VkCapitalUp)
                            {
                                Seconds += _firstAmount;
                            }
                            handled = true;
                            break;
                        }
                        case ButtonOneDown:
                        {
                            var vkey = ((int)lParam >> 16) & 0xFFFF;
                            if (vkey == VkCapitalDown)
                            {
                                Seconds -= _firstAmount;
                            }
                            handled = true;
                            break;
                        }
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void BtnOneAdd_OnClick(object sender, RoutedEventArgs e)
        {
            Seconds += FirstValue;
        }

        private void BtnTwoAdd_OnClick(object sender, RoutedEventArgs e)
        {
            Seconds += SecondValue;
        }

        private void BtnThreeAdd_OnClick(object sender, RoutedEventArgs e) 
        {
            Seconds += ThirdValue;
        }

        private void BtnOneSub_OnClick(object sender, RoutedEventArgs e)
        {
            Seconds -= FirstValue;
        }

        private void BtnTwoSub_OnClick(object sender, RoutedEventArgs e)
        {
            Seconds -= SecondValue;
        }

        private void BtnThreeSub_OnClick(object sender, RoutedEventArgs e)
        {
            Seconds -= ThirdValue;
        }

        private void HandleTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void TxbFirstVal_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(TxbFirstVal.Text, out var result))
            {
                FirstValue = result;
                return;
            }

            FirstValue = 0;
        }

        private void TxbSecondVal_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(TxbSecondVal.Text, out var result))
            {
                SecondValue = result;
                return;
            }

            SecondValue = 0;
        }

        private void TxbThirdVal_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(TxbThirdVal.Text, out var result))
            {
                ThirdValue = result;
                return;
            }

            ThirdValue = 0;
        }
    }
}